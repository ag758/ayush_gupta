+++
title = "About"
template = "about.html"

[extra]
author = "Ayush Gupta"
image = "../photo.jpg"
+++

 Ayush believes in living with small purposes. He wishes to create little impacts through his work that positively influences the life of people around. He thinks through problems critically and has developed a mind of problem solving. Ayush has a passion of building or leading a business sustainably and profitably. He loves to read books that help him to become the better of himself everyday. 